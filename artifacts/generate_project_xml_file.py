import argparse
import xml.etree.ElementTree as ET
from xml.dom import minidom


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="COMPSs")
    parser.add_argument(
        "--workers",
        type=str,
        required=True,
        help="List of workers, such as: worker1,worker2,workerN",
    )
    parser.add_argument(
        "--install_dir",
        type=str,
        required=True,
        help="Path to install dir",
    )
    parser.add_argument(
        "--working_dir",
        type=str,
        required=True,
        help="Path to working dir",
    )
    parser.add_argument(
        "--user",
        type=str,
        required=True,
        help="User name",
    )
    parser.add_argument(
        "--app_dir",
        type=str,
        required=True,
        help="Path to app dir",
    )
    parser.add_argument(
        "--path_to_new_file",
        type=str,
        required=True,
        help="Path to the generated xml file",
    )
    args = parser.parse_args()

    # PROJECT.XML
    workers = str(args.workers).split(',')

    project = ET.Element("Project")
    master_node = ET.SubElement(project, "MasterNode")

    for worker in workers:
        compute_node = ET.SubElement(project, "ComputeNode", Name=worker)
        # compute_node = ET.SubElement(project, "ComputeNode")
        # compute_node.set("Name", f"{worker}")
        install_dir = ET.SubElement(compute_node, "InstallDir").text = args.install_dir
        working_dir = ET.SubElement(compute_node, "WorkingDir").text = args.working_dir
        user = ET.SubElement(compute_node, "User").text = args.user
        application = ET.SubElement(compute_node, "Application")
        # sub
        app_dir = ET.SubElement(application, "AppDir").text = args.app_dir

    xmlstr = minidom.parseString(ET.tostring(project, encoding='UTF-8')).toprettyxml(indent="   ")
    with open(args.path_to_new_file, "w") as f:
        f.write(xmlstr)
