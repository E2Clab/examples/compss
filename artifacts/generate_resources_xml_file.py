import argparse
import xml.etree.ElementTree as ET
from xml.dom import minidom


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="COMPSs")
    parser.add_argument(
        "--workers",
        type=str,
        required=True,
        help="List of workers, such as: worker1,worker2,workerN",
    )
    parser.add_argument(
        "--computing_units",
        type=str,
        required=True,
        help="Computing units",
    )
    parser.add_argument(
        "--memory_size",
        type=str,
        required=True,
        help="Memory size",
    )
    parser.add_argument(
        "--min_port_nio",
        type=str,
        required=True,
        help="Min port NIO",
    )
    parser.add_argument(
        "--max_port_nio",
        type=str,
        required=True,
        help="Max port NIO",
    )
    parser.add_argument(
        "--path_to_new_file",
        type=str,
        required=True,
        help="Path to the generated xml file",
    )
    args = parser.parse_args()

    workers = str(args.workers).split(',')

    resources_list = ET.Element("ResourcesList")

    for worker in workers:
        compute_node = ET.SubElement(resources_list, "ComputeNode", Name=worker)

        processor = ET.SubElement(compute_node, "Processor", Name="MainProcessor")
        computing_units = ET.SubElement(processor, "ComputingUnits").text = args.computing_units
        architecture = ET.SubElement(processor, "Architecture").text = "Intel"
        speed = ET.SubElement(processor, "Speed").text = "2.6"

        memory = ET.SubElement(compute_node, "Memory")
        size = ET.SubElement(memory, "Size").text = args.memory_size

        operating_system = ET.SubElement(compute_node, "OperatingSystem")
        type = ET.SubElement(operating_system, "Type").text = "Linux"
        distribution = ET.SubElement(operating_system, "Distribution").text = "SMP"
        version = ET.SubElement(operating_system, "Version").text = "3.0.101-0.35-default"

        software = ET.SubElement(compute_node, "Software")
        for app_type in ['JAVA', 'PYTHON', 'EXTRAE', 'COMPSS']:
            ET.SubElement(software, "Application").text = app_type

        adaptors = ET.SubElement(compute_node, "Adaptors")

        adaptor_nio = ET.SubElement(adaptors, "Adaptor", Name="es.bsc.compss.nio.master.NIOAdaptor")
        submission_system_nio = ET.SubElement(adaptor_nio, "SubmissionSystem")
        interactive_nio = ET.SubElement(submission_system_nio, "Interactive")
        ports = ET.SubElement(adaptor_nio, "Ports")
        min_port = ET.SubElement(ports, "MinPort").text = args.min_port_nio
        max_port = ET.SubElement(ports, "MaxPort").text = args.max_port_nio

        adaptor_gat = ET.SubElement(adaptors, "Adaptor", Name="es.bsc.compss.gat.master.GATAdaptor")
        submission_system_gat = ET.SubElement(adaptor_gat, "SubmissionSystem")
        interactive_gat = ET.SubElement(submission_system_gat, "Interactive")
        BrokerAdaptor = ET.SubElement(adaptor_gat, "BrokerAdaptor").text = "sshtrilead"

        adaptor_caa = ET.SubElement(adaptors, "Adaptor", Name="es.bsc.compss.agent.comm.CommAgentAdaptor")
        submission_system_caa = ET.SubElement(adaptor_caa, "SubmissionSystem")
        interactive_caa = ET.SubElement(submission_system_caa, "Interactive")
        properties = ET.SubElement(adaptor_caa, "Properties")
        _property = ET.SubElement(properties, "Property")
        vame = ET.SubElement(_property, "Name").text = "Port"
        value = ET.SubElement(_property, "Value").text = "46102"

    xmlstr = minidom.parseString(ET.tostring(resources_list, encoding='UTF-8')).toprettyxml(indent="   ")
    with open(args.path_to_new_file, "w") as f:
        f.write(xmlstr)
