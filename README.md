# COMPSs

Running COMPSs Applications on G5K!

## Tutorial standalone deployment

https://e2clab.gitlabpages.inria.fr/e2clab/examples/compss-standalone.html

## Tutorial Docker-based deployment

https://e2clab.gitlabpages.inria.fr/e2clab/examples/compss-docker.html

